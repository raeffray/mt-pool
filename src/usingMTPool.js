const MTPool = require('./MTPool')

mtPool = new MTPool()

const tenantId_1 = 'localhost-3306-sapdev-DB1';
const tenantId_2 = 'localhost-3306-sapdev-DB2';
const tenantId_3 = 'localhost-5432-sapdev-db1';

const tenantPromisse1 = mtPool.acquire(tenantId_1);
const tenantPromisse2 = mtPool.acquire(tenantId_2);
const tenantPromisse3 = mtPool.acquire(tenantId_3);

tenantPromisse1
    // once we get the connection, query
    .then(client1 => {
        const query = mtPool.buildQuery(tenantId_1)
        return query(client1, "select * from tasks")
    })
    // once the query is done, parse result and release the pool
    .then(({ client: client1, rows }) => {
        rows.forEach(row => {
            console.log("MYSQL DB1: " + row.title)
        });
        mtPool.release(tenantId_1, client1);

    })
    .catch(({ client: client1, err }) => {
        // handle error - this is generally a timeout or maxWaitingClients
        // or error retrieving the data
        mtPool.release(tenantId_1, client1);
    });

tenantPromisse2
    // once we get the connection, query
    .then(client2 => {
        const query = mtPool.buildQuery(tenantId_2)
        return query(client2, "select * from tasks")
    })
    // once the query is done, parse result and release the pool
    .then(({ client: client2, rows }) => {
        rows.forEach(row => {
            console.log("MYSQL DB2: " + row.title)
        });
        mtPool.release(tenantId_2, client2);

    })
    .catch(({ client: client2, err }) => {
        // handle error - this is generally a timeout or maxWaitingClients
        // or error retrieving the data
        mtPool.release(tenantId_2, client2);
    });


tenantPromisse3
    // once we get the connection, query
    .then(client3 => {
        const query = mtPool.buildQuery(tenantId_3)
        return query(client3, "select * from tasks")
    })
    // once the query is done, parse result and release the pool
    .then(({ client: client3, rows }) => {
        rows.forEach(row => {
            console.log("PG DB2: " + row.title)
        });
        mtPool.release(tenantId_3, client3);

    })
    .catch(({ client: client3, err }) => {
        // handle error - this is generally a timeout or maxWaitingClients
        // or error retrieving the data
        mtPool.release(tenantId_3, client3);
    });


mtPool.drain(tenantId_1)
mtPool.drain(tenantId_2)
mtPool.drain(tenantId_3)
