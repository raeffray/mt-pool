const fs = require('fs');

const readFile = (file) => {
    return JSON.parse(fs.readFileSync(file))
}

const readPoolConfig = () => {
    return readFile('./pool-config.json')
}

module.exports = { readPoolConfig, readFile }
