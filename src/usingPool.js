const genericPool = require("generic-pool")

const uuidGenerator = require('uuid');

const { pool: poolConfig } = require('./config/config')

const provider = require('./dbProviders/postgresProvider')
//const provider = require('./dbProviders/mysqlProvider')

const factory = {
    create: () => {
        // called by Pool.js#_createResource
        return provider.create();
    },
    destroy: client => {
        console.log("terminating provider")
        provider.destroy(client)
    }
}

const pool = genericPool.createPool(factory, {
    max: poolConfig.maxSize,
    min: poolConfig.minSize
});

const resourcePromise = pool.acquire();

resourcePromise
    // once we get the connection, query
    .then(client => {
        console.log(`>>> ${uuidGenerator()}`)
        return provider.query(client, "select * from tasks")
    })
    // once the query is done, parse result and release the pool
    .then(({ client, rows }) => {
        console.log(rows)
        pool.release(client);

    })
    .catch(({ client, err }) => {
        // handle error - this is generally a timeout or maxWaitingClients
        // or error retrieving the data
        pool.release(client);
    });

pool.drain().then(() => {
    console.log('draining pool')
    pool.clear();
});