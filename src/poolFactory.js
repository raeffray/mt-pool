const genericPool = require("generic-pool")


const createPool = ((provider, host, port, database, user, password, minConnections, maxConnections) => {

    const factory = {
        create: () => {
            // called by Pool.js#_createResource
            return provider.create(host, port, database, user, password);
        },
        destroy: client => {
            provider.destroy(client)
        }
    }

    const pool = genericPool.createPool(factory, {
        min: minConnections,
        max: maxConnections
    });

    return pool

});

module.exports = createPool

