const pool = require("generic-pool")

const { readPoolConfig } = require('./fileHandler')
const createPool = require('./poolFactory')
const providerResolver = require('./resolveProviders')

const { pools } = readPoolConfig()


class MTPool {

    constructor() {
        this._poolsMap = new Map()
        this.createPools()
    }

    createPools() {
        pools.forEach(({ providerId, host, port, database, user, password, minConnections, maxConnections }) => {
            const _provider = providerResolver(providerId)
            const pool = createPool(_provider, host, port, database, user, password, minConnections, maxConnections)
            const poolKey = `${host}-${port}-${user}-${database}`;

            this._poolsMap.set(poolKey, { pool: pool, provider: _provider })
        });
    }

    acquire(tenantId) {
        const { pool } = this._poolsMap.get(tenantId);
        return pool.acquire()
    }

    drain(tenantId) {
        const { pool } = this._poolsMap.get(tenantId);
        pool.drain().then(() => {
            console.log(`draining pool for tenant ${tenantId}`)
            pool.clear();
        });
    }

    //TODO client is really necessary here??
    release(tenantId, client) {
        const { pool } = this._poolsMap.get(tenantId);
        pool.release(client)
    }

    buildQuery(tenantId) {
        const { provider } = this._poolsMap.get(tenantId);
        return provider.query
    }

}

module.exports = MTPool;

