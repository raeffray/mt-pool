const { Client } = require("pg")

const provider = {
    create: (host, port, database, user, password) => {
        const connection = new Client({
            host: host,
            port: port,
            database: database,
            user: user,
            password: password
        })
        connection.connect()
        return connection
    },

    destroy: client => {
        client.end()
    },

    query: (_client, sql) => {
        this.client = _client;
        return new Promise((resolve, reject) => {
            this.client.query(sql, (err, results) => {
                if (err) {
                    reject({ client: _client, err: err });
                } else {
                    resolve({ client: _client, rows: results.rows })
                }
            })
        })
    }

}
module.exports = provider

