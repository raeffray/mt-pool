const driver = require('mysql')

const provider = {
    create: (host, port, database, user, password) => {
        return driver.createConnection(
            {
                host: host,
                port: port,
                database: database,
                user: user,
                password: password
            }
        )
    },

    destroy: client => {
        client.end()
    },

    query: (_client, sql) => {
        this.client = _client;
        return new Promise((resolve, reject) => {
            this.client.query(sql, (err, results, fields) => {
                if (err) {
                    reject({ client: _client, err: err });
                } else {
                    resolve({ client: _client, rows: results })
                }
            })
        })
    }
}
module.exports = provider
