
const resolveProvider = (providerName) => { return require(`./dbProviders/${providerName}`) }

module.exports = resolveProvider