# mt-pool

### Installing:

```npm install```

### Watching:

```npm run watch src/usingPool.js```

### Creating DBs

```docker-compose create``` or ```docker-compose up```